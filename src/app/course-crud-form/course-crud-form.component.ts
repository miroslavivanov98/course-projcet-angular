import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-course-crud-form',
  templateUrl: './course-crud-form.component.html',
  styleUrls: ['./course-crud-form.component.less']
})
export class CourseCrudFormComponent implements OnInit {

  @Input() modelId;
  @Output() formSubmit = new EventEmitter<object>();

  titleValue = "";
  priceValue = 0.00;
  descriptionValue = "";

  constructor(private service: CoursesService) {}

  formSubmitted(e, name, price, description) {
    e.preventDefault();
    
    // console.log('Form submitted', name, price, description); 
    this.formSubmit.emit({name, description, price: parseFloat(price)});
  }

  ngOnInit() {
    if(this.modelId) {
      let product = this.service.getById(parseInt(this.modelId));

      this.titleValue = product.name;
      this.priceValue = product.price;
      this.descriptionValue = product.description;
    }
  }

}
