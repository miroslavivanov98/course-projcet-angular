import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCrudFormComponent } from './course-crud-form.component';

describe('CourseCrudFormComponent', () => {
  let component: CourseCrudFormComponent;
  let fixture: ComponentFixture<CourseCrudFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCrudFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCrudFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
