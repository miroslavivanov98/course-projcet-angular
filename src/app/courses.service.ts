export class CoursesService{
    private courses = [
        { name: "Angular", id : 1, price: 16.99, description: "Angular Full course"},
        { name: "React+Redux", id : 2, price: 29.99, description: "Learn React, Redux, GraphQL and NodeJS"},
        { name: "Python", id : 3, price: 4.99, description: "Python beginners course"}
    ]

    constructor() {
        this.loadCourses();
    }

    saveCourses() {
        localStorage.setItem("courses", JSON.stringify(this.courses));
    }

    loadCourses() {
        if(localStorage.getItem("courses")) 
            this.courses = JSON.parse(localStorage.getItem("courses"));
    }

    getCourses(){
        return this.courses;
    }

    getById(id) {
        return this.courses.filter(x => x.id == id)[0];
    }

    create(data) {
        data.id = this.courses.length + 1;

        this.courses.push(data);

        this.saveCourses();
        return data.id;
    }

    update(id, data) {
        let course = this.getById(id);
        course = Object.assign(course, data); 

        this.courses = this.courses.filter(x => x.id != id);
        this.courses.push(course);
        this.saveCourses();
    }

    delete(id) {
        this.courses = this.courses.filter(x => x.id != id);
        this.saveCourses();
    }
}