import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCrudUpdateComponent } from './course-crud-update.component';

describe('CourseCrudUpdateComponent', () => {
  let component: CourseCrudUpdateComponent;
  let fixture: ComponentFixture<CourseCrudUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCrudUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCrudUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
