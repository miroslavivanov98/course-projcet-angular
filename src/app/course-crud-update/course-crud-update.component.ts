import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-course-crud-update',
  templateUrl: './course-crud-update.component.html',
  styleUrls: ['./course-crud-update.component.less']
})
export class CourseCrudUpdateComponent implements OnInit {

  modelId = "1";

  constructor(private route: ActivatedRoute, private service: CoursesService, private router: Router) {
  }
  
  
  ngOnInit(): void {
    this.modelId = this.route.snapshot.paramMap.get('courseId');
  }
  
  formSubmitted(data) {
    this.service.update(this.modelId, data);
    this.router.navigate(['/courses', this.modelId]);
  }

}
