import { Component, OnInit } from '@angular/core';
import { EmailService } from '../email.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit {

  items;
  sum;
  constructor( private service: EmailService ) {
    this.items = service.getItems();
  }

  ngOnInit(): void {
    this.calculateTotal();
  }

  removeItem(id) {
    this.items = this.service.removeItem(id);
    this.calculateTotal();
  }
  clearCart(){
    this.items = this.service.clearCart();
    this.calculateTotal();
  }

  addAmount(itemId, amount) {
    this.items = this.service.addAmount(itemId, amount);
    this.calculateTotal();
  }

  calculateTotal() {
    let sum = 0;
    for(let item of this.items) {
      sum += item.price * item.amount;
    }

    this.sum = sum;
  }

}
