import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.less']
})
export class CounterComponent implements OnInit {

  constructor() { }

  @Input() min: number = 0;
  @Input() max: number = 10;
  @Input() counter: number = 5;

  ngOnInit(): void {
  }

  counterMin(){
    if(this.counter <= this.min){
      this.counter = this.min
    }
    else{
      this.counter--;
    }

  }
  counterMax(){
    if(this.counter >= this.max) {
      this.counter = this.max;
    }
    else {
      this.counter++;
    }
  }
}
