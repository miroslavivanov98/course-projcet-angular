import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-course-crud-delete',
  templateUrl: './course-crud-delete.component.html',
  styleUrls: ['./course-crud-delete.component.less']
})
export class CourseCrudDeleteComponent implements OnInit {

  course;
  courseId;
  constructor(private route: ActivatedRoute, private service: CoursesService, private router: Router) {
    this.courseId = this.route.snapshot.paramMap.get('courseId');

    this.course = service.getById(this.courseId);
  }
  
  ngOnInit(): void {
  }

  confirm() {
    this.service.delete(this.courseId);
    this.router.navigate(['/courses']);
  }

}
