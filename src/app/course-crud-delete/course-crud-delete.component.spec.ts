import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCrudDeleteComponent } from './course-crud-delete.component';

describe('CourseCrudDeleteComponent', () => {
  let component: CourseCrudDeleteComponent;
  let fixture: ComponentFixture<CourseCrudDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCrudDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCrudDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
