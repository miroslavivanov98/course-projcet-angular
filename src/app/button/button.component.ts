import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less']
})
export class ButtonComponent {

  constructor() { }

  clickedAmount = 0;
  @Input() type: string = "primary";
  @Input() text: string = "I am a primary button";

  @Output() clicked = new EventEmitter<string>();

  buttonClicked() {
    this.clickedAmount++;
    this.clicked.emit(`You clicked Miro's button ${this.clickedAmount} times`);
  }

}
