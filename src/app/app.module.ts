import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {CoursesComponent} from './courses.component'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { CoursesService } from './courses.service';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { CourseCrudCreateComponent } from './course-crud-create/course-crud-create.component';
import { CourseCrudUpdateComponent } from './course-crud-update/course-crud-update.component';
import { CourseCrudDeleteComponent } from './course-crud-delete/course-crud-delete.component';
import { CourseCrudFormComponent } from './course-crud-form/course-crud-form.component';
import { PlaygroundComponent } from './playground/playground.component';
import { ButtonComponent } from './button/button.component';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CourseComponent,
    PageNotFoundComponent,
    HomeComponent,
    CartComponent,
    CourseCrudCreateComponent,
    CourseCrudUpdateComponent,
    CourseCrudDeleteComponent,
    CourseCrudFormComponent,
    PlaygroundComponent,
    ButtonComponent,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'home', component: HomeComponent },
      { path: 'courses/new', component: CourseCrudCreateComponent },
      { path: 'courses/:courseId/edit', component: CourseCrudUpdateComponent },
      { path: 'courses/:courseId/delete', component: CourseCrudDeleteComponent },
      { path: 'courses/:coursesId', component: CourseComponent },
      { path: 'courses', component: CoursesComponent },
      { path: 'cart', component: CartComponent },
      { path: 'playground', component: PlaygroundComponent },
      { path : 'counter', component: CounterComponent},
      { path: '**', component: PageNotFoundComponent },
    ])
  ],
 
  providers: [
    CoursesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
