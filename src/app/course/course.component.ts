import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmailService } from '../email.service';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.less']
})
export class CourseComponent implements OnInit {

  course;
  amountElement: any;

  constructor(
    public route: ActivatedRoute,
    public service: EmailService,
    private courseService: CoursesService
  ) {
    
    let courseId = route.snapshot.paramMap.get('coursesId');
    this.course = courseService.getById(courseId);

  }

  ngOnInit(): void {
  }

  addToCart(product) {
    let amount = 1;
    if(this.amountElement != null) {
      amount = Math.floor(this.amountElement.value);
    }

    if(!this.validateAmount(amount)) {
      window.alert('Invalid amount!');
      return;
    }

    product.amount = amount;

    this.service.addToCart(product);
    window.alert(`You selected ${amount} items`);
    // window.alert("Your product has been added to the card!");
  }

  validateAmount(amt) {
    return (Math.floor(amt) > 0)
  }

  onAmountChange(event: any) {
    this.amountElement = event.target;
  }

}
