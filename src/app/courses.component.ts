import { Component} from '@angular/core';
import {CoursesService} from './courses.service'

@Component({
    selector: 'courses',
    template: `
        
        <h2>{{title}}</h2>
        <ul>
            <li *ngFor="let course of courses"> 
            <a [title]="course.name + ' details'" [routerLink]="['/courses', course.id]">
                {{ course.name }}
            </a>
            </li>
        </ul>
    `
})
export class CoursesComponent {
    title = "List of courses"
    courses;

    constructor(service: CoursesService){
        this.courses = service.getCourses();
    }
}