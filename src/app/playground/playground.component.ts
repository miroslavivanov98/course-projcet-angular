import { Component, Input, Output } from '@angular/core';
import { CoursesService } from '../courses.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.less']
})
export class PlaygroundComponent {

  constructor(private service: CoursesService) { }

  ngOnInit(): void {
  }

  formSubmitted(data) {
    console.log("Playground page received the data:", data); 
  }
  showAlert(text){
    window.alert(text);
    window.alert(";)");
  }

  counters = [
    {id:1, min: 0, max: 10, count: 5},
    {id:2, min: 0, max: 20, count: 10}
  ]

}
