import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../courses.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course-crud-create',
  templateUrl: './course-crud-create.component.html',
  styleUrls: ['./course-crud-create.component.less']
})
export class CourseCrudCreateComponent implements OnInit {

  constructor(private service: CoursesService, private router: Router) { }

  ngOnInit(): void {
  }

  formSubmitted(data) {
    // Create;
    let id = this.service.create(data);
    this.router.navigate(['/courses', id]);
  }

}
