import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseCrudCreateComponent } from './course-crud-create.component';

describe('CourseCrudCreateComponent', () => {
  let component: CourseCrudCreateComponent;
  let fixture: ComponentFixture<CourseCrudCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseCrudCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseCrudCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
