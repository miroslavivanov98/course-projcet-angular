import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor() {
    this.loadCart();
  }

  items = [
    
  ];

  saveCart() {
    localStorage.setItem("cart", JSON.stringify(this.items));
  }

  loadCart() {
    if(localStorage.getItem("cart"))
      this.items = JSON.parse(localStorage.getItem("cart"));
  }
   
  addToCart(course){
    if(this.items.find(x => x.id == course.id)) {
      // if exist
      let oldAmount = this.items.find(x => x.id == course.id).amount;

      this.removeItem(course.id);
      course.amount += oldAmount;
    }
    this.items.push(course);
    this.saveCart()
  }
  getItems(){
    return this.items;
  }
  clearCart(){
    this.items = [];
    this.saveCart()
    return this.items;
  }
  removeItem(id: number) {
    this.items = this.items.filter(x => x.id != id);
    this.saveCart()
    return this.items;
  }

  findById(id: number) {
    return this.items.find(x => x.id == id);
  }

  addAmount(itemId, amount) {
    let item = this.findById(itemId);
    item.amount += amount;
    this.saveCart()
    return this.items;
  }

}
